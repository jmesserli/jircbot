/**************************************************************************************************
 * JIRCBot Copyright (C) 2013 Joel Messerli - JMNetwork.ch                                        *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package ch.jmnetwork.ircbot;

import org.jibble.pircbot.IrcException;

import java.io.IOException;

/**
 * User: joel / Date: 17.12.13 / Time: 22:10
 */
public class Main {

    public static void main(String[] args) throws IrcException, IOException {
        JBot bot = new JBot("TH3Bot", "TH3N00B");

        bot.setVerbose(true);
        bot.connect("irc.esper.net", 5555);
        bot.joinChannel("#TH3N00B");

        bot.sendMessage("#TH3N00B","Hello, I'm TH3Bot, say TH3Bot help for more info.");
    }
}
