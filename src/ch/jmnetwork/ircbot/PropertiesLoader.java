/**************************************************************************************************
 * JIRCBot Copyright (C) 2013 Joel Messerli - JMNetwork.ch                                        *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package ch.jmnetwork.ircbot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

/**
 * User: joel / Date: 17.12.13 / Time: 22:36
 */
public class PropertiesLoader {

    Properties properties = new Properties();
    File file;

    public PropertiesLoader(File file) throws IOException {
        this.file = file;

        if (!file.exists()) file.createNewFile();
    }

    public HashMap<String, String> loadCommandList() throws IOException {
        properties.load(new FileInputStream(file));

        HashMap<String, String> tempMap = new HashMap<>();

        for (Object s : properties.keySet()) {
            String st = (String) s;
            tempMap.put(st, (String) properties.get(s));
        }

        return tempMap;
    }

    public void saveCommands(HashMap<String, String> commandMap) throws IOException {
        properties.clear();

        for (String s : commandMap.keySet()) {
            properties.put(s, commandMap.get(s));
        }

        properties.store(new FileOutputStream(file), "");
    }
}
