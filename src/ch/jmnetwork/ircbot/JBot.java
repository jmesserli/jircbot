/**************************************************************************************************
 * JIRCBot Copyright (C) 2013 Joel Messerli - JMNetwork.ch                                        *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package ch.jmnetwork.ircbot;

import org.jibble.pircbot.PircBot;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * User: joel / Date: 17.12.13 / Time: 22:10
 */
public class JBot extends PircBot {

    String permittedSender;
    HashMap<String, String> cmdRespMap;
    List<String> adminCMDList = new ArrayList<>();
    PropertiesLoader pl;

    public JBot(String nick, String permittedSender) throws IOException {
        setName(nick);
        setLogin(nick);
        this.permittedSender = permittedSender;
        pl = new PropertiesLoader(new File("botCommands"));
        cmdRespMap = pl.loadCommandList();
        addDefaultComms();
    }

    @Override
    protected void onMessage(String channel, String sender, String login, String hostname, String message) {

        if (message.startsWith(getNick())) {
            if (message.contains("ahelp") && sender.equals(permittedSender)) {
                String commands = "";
                for (String s : adminCMDList) {
                    commands += s + " / ";
                }
                sendMessage(channel, "JIRCBot Commands: " + commands);
            } else if (message.contains("help")) {
                String commands = "";
                for (String s : cmdRespMap.keySet()) {
                    commands += s + " / ";
                }
                sendMessage(channel, "JIRCBot Commands: " + commands);
            } else if (message.contains("stop") && sender.equals(permittedSender)) {
                disconnect();
                System.exit(0);
            } else if (message.contains("addcom") && sender.equals(permittedSender)) {
                String command = message.split(":")[0].split(" ")[2].trim();
                String response = message.split(":")[1].trim();

                sendMessage(channel, sender + ": Adding command " + command + " (" + response + ")");

                cmdRespMap.put(command, response);
            } else if (message.contains("delcom") && sender.equals(permittedSender)) {
                String command = message.split(" ")[2];

                sendMessage(channel, sender + ": Removing command " + command + " (" + cmdRespMap.get(command) + ")");

                cmdRespMap.remove(command);
            } else if (message.contains("savecoms") && sender.equals(permittedSender)) {
                try {
                    pl.saveCommands(cmdRespMap);
                    sendMessage(channel, sender + ": Save commands complete.");
                } catch (IOException e) {
                    sendMessage(channel, sender + ": Failed saving commands: " + e.getMessage());
                }
            } else if (message.contains("reload") && sender.equals(permittedSender)) {
                try {
                    pl = new PropertiesLoader(new File("botCommands"));
                    cmdRespMap = pl.loadCommandList();
                    addDefaultComms();
                    sendMessage(channel, sender + ": Reload complete.");
                } catch (IOException e) {
                    sendMessage(channel, sender + ": Failed reloading: " + e.getMessage());
                }
            } else {
                for (String s : cmdRespMap.keySet()) {
                    System.err.println("Trying to match " + message.split(" ")[1].trim() + " with " + s);
                    if (message.split(" ").length > 1 && message.split(" ")[1].trim().equals(s)) {
                        sendMessage(channel, cmdRespMap.get(s));
                        return;
                    }
                }
                sendMessage(channel, sender + ": I don't know '" + message.split(" ")[1].trim() + "'");
            }
        }
    }

    private void addDefaultComms() {
        adminCMDList.clear();
        adminCMDList.add("addcom cmd:response");
        adminCMDList.add("delcom cmd");
        adminCMDList.add("savecoms");
        adminCMDList.add("reload");
    }
}
